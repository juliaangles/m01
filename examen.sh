#!/bin/bash
#julia angles
#20232-2024
#06-03-2024
#
#
#-------------------------------------------------------
#

#1)validar arguments

ARGSOK=1

if [ $# -eq 1 ]
then
	echo "Error: num args incorrecte"
	echo "Usage $0 file"
	exit $ARGSOK
fi

#2)validar si es dir
NODIR=0
dir=$1
if [ ! -d $dir ]
	then 
		echo "Error: $1 no es un dir" >> /dev/stderr
		NODIR=2
		#exit $NODIR		

fi

#3) xixa
#
noexecutable=0
file=$2
for file in $*
do
	if [ ! -e $file ]
	then
		echo "$file no existeix"

	else

	 	file -b $file | grep -iq "elf" | cut -c1,2,3
 		if [ $? -eq 0 ]
		then 
			cp $2 $1
			echo $file
		else
			echo "$file" >> /dev/stderr
			((noexecutable++))
			NOEX=3
			exit $NOEX
		fi
	fi 
	 
done
echo "Total fitxers no executable-binaris: $noexecutable"
exit 0
