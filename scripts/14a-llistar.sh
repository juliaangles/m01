#! /bin/bash
# julia
# gener
# prog dir
# rep un argument i es un directori i es llista
# ----------------------------------------------------------------------------

#1) validar arg
ERR_NARGS=1

if [ $# -ne 1 ]
then
	echo "ERROR: num args incorrecte"
	echo "Usage: $0 "
	exit $ERR_NARGS
fi

#2)validar q es dir

dir=$1
ERR_NODIR=2

if [ ! -d $dir ]
then
	echo "ERROR: $1 no es un directori"
	echo "Usage: $0 dir"
	exit $ERR_NODIR
fi

#3)xixa

ls $dir
exit 0
