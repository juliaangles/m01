#! /bin/bash
# julia
# gener
# prog dir
# a)rep un argument i es un directori i es llista
# b) llistar numerant els elements del dir
# ----------------------------------------------------------------------------

#1) validar arg
ERR_NARGS=1

if [ $# -ne 1 ]
then
	echo "ERROR: num args incorrecte"
	echo "Usage: $0 "
	exit $ERR_NARGS
fi

#2)validar q es dir

dir=$1
ERR_NODIR=2

if [ ! -d $dir ]
then
	echo "ERROR: $1 no es un directori"
	echo "Usage: $0 dir"
	exit $ERR_NODIR
fi

#3)xixa

llista=$(ls $dir)
compt=1

for elem in $llista
do
	echo "$compt: $elem"
	((compt ++))

done
exit 0
