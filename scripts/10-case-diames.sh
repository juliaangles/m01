#! /bin/bash
#@ julia ASIX-M01 
#Curs 2023-2024
#Febrer 2024
#Descripcio: dir els dies que te un mes
#Synopsis: progm mes
#	a) validar rep un argument
#	b) validar mes [1-12]
#	c) xixa --> case
#--------------------------------------
ERR_NARGS=1
ERR_MES=2

#1)Validar argument
if [ $# -ne 1 ]
then 
	echo "Error:num d'arguments incorrecte"
	echo "Usage: $0 mes"
	exit $ERR_NARGS
fi

#b) validar mes
mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]
	echo "Error: mes $mes incorrecte"
	echo "Els mesos van del 1 al 12"
	echo "Usage: $0 mes"
	exit $ERR_MES
fi

#c) xixa
case $mes in
	"2")
		echo "El mes $mes te 28 dies"
		;;
	"4"|"6"|"9"|"11")
		echo "El mes $mes te 31 dies"
		;;
	*)
		echo "El mes $mes te 30 dies"
		;;
esac
exit 0

