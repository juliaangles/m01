#! /bin/bash
# julia
# gener
# prog dir
# a)rep un argument i es un directori i es llista
# b) llistar numerant els elements del dir
# c) per cada element dir si es dir, regular, o altra cosa
# ----------------------------------------------------------------------------

#1) validar arg
ERR_NARGS=1

if [ $# -ne 1 ]
then
	echo "ERROR: num args incorrecte"
	echo "Usage: $0 "
	exit $ERR_NARGS
fi

#2)validar q es dir

dir=$1
ERR_NODIR=2

if [ ! -d $dir ]
then
	echo "ERROR: $1 no es un directori"
	echo "Usage: $0 dir"
	exit $ERR_NODIR
fi

#3)xixa

llista=$(ls $dir)

for elem in $llista
do
	if [ -d $dir/$elem ]
	then
		echo "$elem Es un directori"
	elif [ -f $dir/$elem ]
	then
		echo "$elem Es un regular file"
	else
		echo "$elem Es una altra cosa"

	fi
done
exit 0
