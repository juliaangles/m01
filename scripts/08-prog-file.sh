#! /bin/bash
#@ julia ASIX-M01 Curs 2023-2024
#Febrer 2024
#Indicar si dir es: regular, dir, link o altre cosa
#Validar que s'entra un argument

#1) verificar rep un arg
ERR_ARGS=1
ERR_NODIR=2

if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_ARGS
fi

#2 ) indicar tipus
fit=$1

if [ -f $1 ]
then
    echo "El $1 es un regular file"
elif [ -d $1 ]
then
    echo "El $1 es un directori"
elif [ -h $1 ]
then
    echo "El $1 es un link"
else
    echo "El $1 es una altre cosa"

fi
exit 0
