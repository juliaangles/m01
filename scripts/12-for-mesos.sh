#!/bin/bash
#@julia ASIX-M01
#Febrer 2024
#Programa notes: Programa que rep almenys una nota o més  i per cada nota diu si és un #suspès, aprovat, notable o excel·lent.
#
#------------------------------------------------------------------------------------

ERR_NARGS=1
ERR_ARGVL=2

#1) Validar arguments

if [ $# -eq 0 ]
then
  echo "Error: num args incorrecte"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi

#2)Validar rang de nota
for mes in $*
do
    if ! [ $mes 
    fi
done
