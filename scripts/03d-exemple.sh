#! /bin/bash

#@julia ASIX-M01
#Gener 2024
#
#Exemples ordres if
#
#------------------------------------------------------------------------------------

#1) Validar arguments
if [ $# -ne 1 ]
then
  echo "Error: num args incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

#2)xixa

edat=$1

if [ $edat -lt 18 ]
then
  echo "edat $edat es menor d'edat"

elif[ $edat -lt 65 ]
then
  echo "edat $edat es edat activa"

else
  echo "edat $edat es edat de jubilació"
fi
exit 0
