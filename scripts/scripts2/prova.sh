#!/bin/bash
#prog.sh file
#
#----------------------------------------------------
ERR_NARGS=1

#1) Validar arguments
if [ $# -eq 0 ]
then
    echo "Error: num args incorrecte"
    echo "Usage: $0 matricules "
    exit $ERR_NARGS
fi

#2)xixa
#
status=0
fileIN=$1

while read -r line
do
	echo $line | grep  -E "^[A-Z,a-z]{4}[0-9]{3}$"

	if [ $? -ne 0 ]
	then
		echo "$line" >> /dev/stderr
		status=3
	fi
done < $fileIN
exit $status	
