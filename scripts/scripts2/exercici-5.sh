
#!/bin/bash
#@julia ASIX-M01
#Gener 2024
#FULLA 2
#2. Processar els arguments i comptar quantes n'hi ha de 3 o mes caracters.
#-------------------------------------------------------------------------------
ERR_NARGS=1

#1) Validar arguments
if [ $# -eq 0 ]
then
    echo "Error: num args incorrecte"
    echo "Usage: $0"
    exit $ERR_NARGS
fi

#2) xixa

while read -r line
do	
	chars=$(echo $line | wc -c)
	if [ $chars -lt 50 ]
	then
	      echo $line
	fi
done
exit 0
