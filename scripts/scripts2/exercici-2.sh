#!/bin/bash
#@julia ASIX-M01
#Gener 2024
#FULLA 2
#2. Processar els arguments i comptar quantes n'hi ha de 3 o mes caracters.
#-------------------------------------------------------------------------------
ERR_NARGS=1

#1) Validar arguments
if [ $# -eq 0 ]
then
    echo "Error: num args incorrecte"
    echo "Usage: $0"
    exit $ERR_NARGS
fi

#2) xixa
compt=0
for arg in $*
do	
	echo $arg | grep -q -E "^.{3,}"
	if [ $? -eq 0 ]
	then
	      ((compt++))
	fi
done
echo $compt
exit 0

