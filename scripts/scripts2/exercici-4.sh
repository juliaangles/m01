#!/bin/bash
#@julia ASIX-M01
#Gener 2024
#FULLA 2
#4.Processar stdin mostrant les linies numerades i en majuscules
#-------------------------------------------------------------------------------
ERR_NARGS=1

#1) Validar arguments
if [ $# -eq 0 ]
then
    echo "Error: num args incorrecte"
    echo "Usage: $0 matricules "
    exit $ERR_NARGS
fi

#2)xixa
cont=1

while read -r line
do 
	((count++))
       echo "$count: $line" | tr '[:lower:]' '[:upper:]'
done
exit 0
