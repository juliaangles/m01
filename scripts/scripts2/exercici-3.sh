#!/bin/bash
#@julia ASIX-M01
#Gener 2024
#FULLA 2
#3.Processar arguments que son matricules:
#	a)llistar les valides, del tipus: 9999-AAA
#	b)stdout les que son valides, per stderr les no valides. Retorna de 
#	status el numero d'errors (no valides)
#-------------------------------------------------------------------------------
ERR_NARGS=1

#1) Validar arguments
if [ $# -eq 0 ]
then
    echo "Error: num args incorrecte"
    echo "Usage: $0 matricules "
    exit $ERR_NARGS
fi

#2)xixa
status=0

for matricula in $*
do
	echo $matricula | grep -E -q "^[0-9]{4}-[A-Z]{3}$"

	if [ $? -ne 0 ]
	then
		((status++))
		echo "La matricula $matricula no es valida" >> /dev/stderr
	fi
done
exit $status

