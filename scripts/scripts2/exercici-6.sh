#!/bin/bash
#@julia ASIX-M01
#Gener 2024
#FULLA 2
#6.Processaar per stdin linies d'entrada topus "Tom Snyder" i mostrar per stdout la linia en format --> T.Snyder
#-------------------------------------------------------------------------------
ERR_NARGS=1

while read -r line
do
	nom=$(echo $line | cut -c1)
	cognom=$(echo $line | cut -d ' ' -f2)

	echo "$nom"."$cognom"

done
exit 0
