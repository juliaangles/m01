#!/bin/bash
#@julia ASIX-M01
#Gener 2024
#FULLA 2
#1. Processar els arguments i mostrar per stdout nomes els de 4 o mes caracters.
#-------------------------------------------------------------------------------
ERR_NARGS=1

#1) Validar arguments
if [ $# -eq 0 ]
then
    echo "Error: num args incorrecte"
    echo "Usage: $0"
    exit $ERR_NARGS
fi

#2) xixa

for arg in $*
do	
	echo $arg | grep -E "^.{4,}"

done
exit 0

