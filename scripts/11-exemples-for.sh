#! /bin/bash
#@ julia ASIX-M01 
#Curs 2023-2024
#Febrer 2024
#Descripcio:exemples for
#--------------------------------------
#8)llistar logins ordenats i enumerar
llistat_logins=$(cut -d: -f1 /etc/passwd | sort -g)
num=1
for elem in $llistat_logins
do
   echo "$num:$elem"
   num=$((num+1))
done
exit 0

#7) llistar numerant les lineas
llistat=$(ls)
num=1
for elem in $llistat
do
   echo "$num:$elem"
   num=$((num+1))
done
exit 0

#6)iterar pel resultat d'executar la ordre ls
llistat=$(ls)
for elem in $llistat
do
   echo "$elem"
done

exit 0

#5)numerar arguments
num=1

for arg in $*
do 
   echo "$num:$arg"
   num=$((num+1))
done
exit 0

#4)$@ expandeix $* no
for arg in "$@"
do 
   echo "$arg"
done
exit 0

#
#3)iterer per la llista d'arguments
for arg in "$*"
do 
   echo "$arg"
done
exit 0
#
#2)
#
for nom in "pere marta anna pau"
do
  echo "$nom"
done
exit 0

#1) interar noms
for nom in "pere" "marta" "anna" "pau"
do
  echo "$nom"
done
exit 0
