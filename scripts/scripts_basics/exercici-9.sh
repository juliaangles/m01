#! /bin/bash
#@julia ASIX-M01
#Gener 2024
#9.Fer un programa que rep per stdin noms d'usuari(un per linia), si existeixen en el sistema imostra el nom per stdout. Si no existeis el mostra per stderr.
#
#----------------------------------------------------------------------------------------------
ERR_NARGS=1

#1) Validar arguments
if [ $# -eq 0 ]
then
    echo "Error: num args incorrecte"
    echo "Usage: $0 noms"
    exit $ERR_NARGS
fi

#2)xixa

while read -r line
do
	grep -iq "^$line:" /etc/passwd	
	
	if [ $? -eq 0 ]
	then
		echo "$line"
	else
		echo "ERROR: El usuari no esta al fitxer" >> /dev/stderr
	fi
done
exit 0
