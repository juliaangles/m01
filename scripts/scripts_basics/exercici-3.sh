#! /bin/bash
#@julia ASIX-M01
#Gener 2024
#3. fer un comptador des de zero fins al valor indicat per l'argument rebut
#
#-------------------------------------------------------------------------------
compt=0
MAX=$1

while [ $compt -le $MAX ]
do
	echo -n "$compt "
	((compt++))
done
exit 0
