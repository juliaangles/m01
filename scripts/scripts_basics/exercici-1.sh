#! /bin/bash
#@julia ASIX-M01
#febrer 2024
#1.mostrar la entrada estandar numerant linia a linia
#
#-------------------------------------------------------------------------------
compt=1

while read -r line
do
	echo "$compt: $line"
	((compt++))
done
exit 0
