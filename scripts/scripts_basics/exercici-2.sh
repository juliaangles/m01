#! /bin/bash
#@julia ASIX-M01
#Gener 2024
#2. mostrar els arguments rebuts linia a linia, tot numerant-los
#
#-------------------------------------------------------------------------------
compt=1
for arg in $*
do
   echo "$compt": "$arg"
   ((compt++))
done
exit 0

