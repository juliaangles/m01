#! /bin/bash
#@julia ASIX-M01
#Gener 2024
#7.Processar linia a lina l'entrada estandar, si la linia te mes de 60 caracters la mostra, si no no.
#
#-------------------------------------------------------------------------------------------

while read -r line
do
	chars=$(echo $line | wc -c)
	if [ $chars -gt 60 ]
	then
	echo "$line"
	fi

done
exit 0

