#!/bin/bash
#@julia ASIX-M01
#Gener 2024
#6.Fer un programa que rep com argument noms de dies de la setmana i mostra quants dies eren laborals i quants festius. Si l'argument no es un dia de la setama genera un error per stderr.
#-------------------------------------------------------------------------------

ERR_NARGS=1

#1) Validar arguments
if [ $# -eq 0 ]
then
    echo "Error: num args incorrecte"
    echo "Usage: $0 dia setmana"
    exit $ERR_NARGS
fi
#2)dia setmana
festius=0
laborals=0

for dia in "$@"
do
    case $dia in
        "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
            ((laborals++))
	    ;;
        "dissabte"|"diumenge")
            ((festius++))
	     ;;
        *)
            echo "Error: '$dia' aixo no es un dia de la setmana valid " >> /dev/stderr
	    ;;
    esac
done
echo "Dies laborals: $laborals"
echo "Dies festius: $festius" 

exit 0
