#! /bin/bash
#@julia ASIX-M01
#Febrer 2024
#
#4.Fer un programa que rep com a argument numero de un mes (un o mes) i indica 
#per a cada mes rebut quants dies te el mes.
#-------------------------------------------------------------------------------

ERR_NARGS=1
ERR_ARGVL=2

#1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num args incorrecte"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi
#2)dia del mes
mes=$1 
for mes in "$@"
do
  if ! [ $mes -ge 1 -a $mes -le 12 ]
  then
	  echo "Error: el mes $mes no es valid, ha de estar entre el 1 y el 12" >> /dev/stderr
	 
  else
	case $mes in
		"2")
	    		dies=28;;
		"4"|"6"|"9"|"11")
	    		dies=30;;
		*)
	    		dies=31;;
	esac
	echo "El mes $mes, te $dies dies"
  fi

done

exit 0







