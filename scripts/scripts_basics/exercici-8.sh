#! /bin/bash
#@julia ASIX-M01
#Gener 2024
#8.Fer un programa que rep com a argument noms d'usuaris, si existeixen en el sistema mostra el nom per stdout. Si no existeis el mostra per stderr.
#
#-------------------------------------------------------------------------------------

ERR_NARGS=1

#1) Validar arguments
if [ $# -eq 0 ]
then
    echo "Error: num args incorrecte"
    echo "Usage: $0 noms"
    exit $ERR_NARGS
fi

#2)xixa

for user in $* 
do
	 grep -iq "^$user:" /etc/passwd

	if [ $? -eq 0 ]
	then
		echo "$user"
	else
		echo "ERROR: El usuari no esta al fitxer" >> /dev/stderr
	fi
done
exit 0

