#! /bin/bash
#@julia ASIX-M01
#Gener 2024
#5.Mostrar linia a linia l'entrada estandar, retallant nomes els primers 50 caracters
#
#-------------------------------------------------------------------------------------

while read -r line 
do 
	echo "$line" | cut -c 1-50
done
exit 0
