#! /bin/bash
#exemples de funcions de disc
#abril 2024
#julia angles
#(1)fsize Donat un login calcular amb du l'ocupació del home de l'usuari. Cal obtenir el home del /etc/passwd.
#(2)loginargs Aquesta funció rep logins i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsize. Verificar que es rep almenys un argument. Per a cada argument verificar si és un login vàlid, si no generra una traça d'error.
#------------------------------------------------------------

function fsize(){
	login=$1
	line=$(grep "^$login:" /etc/passwd)
	if [ -z "$line" ]
	then
	   echo "error: ....."
	   return 1
	fi
	dirHome=$(echo $line | cut -d: -f6)
	du -sh $dirHome
}

#2)rep arguments que son logins i calcula el fsize del home
function loginargs(){
	if [ $# -eq 0 ]
	then
	    echo "error: num args"
	    return 1
	fi
	for login in $*
	do
	    #cridar funcio
	    fsize $login
	done
}

#3)rep un argument que es un fitxer i cada linia del fitxer te un login
function loginfile(){
	if [ ! -f $1 ]
	then
	    echo "Error: no existeix"
	    return 1
	fi
	fileIn=$1
	while read -r login
	do
	   fsize $login
	done < $fileIn
}

#rep login per la entrada standar, per a cada un d'ells calcula fsize
function loginstdin(){
	while read -r login
	do
		fsize $login
	done
	
}
#precessa el fitxer rebut o per stdin si no en rep cap
function loginboth(){
	fileIn="/dev/stdin"
	
	if [ $# -eq 1 ]
	then
		fileIn=$1
	fi

	while read -r login
	do
	   fsize $login
	done < $fileIn
}
