#!/bin/bash
#@julia ASIX-M01
#Febrer 2024
#Exemples while
#
#------------------------------------------------------------------------------------
#  echo -n "$num " Per que no salti de linea
#
#3) iterar la llista d'arguments (ojo utilitzar normalment for)
while [ -n "$1" ]
do
  echo "$1 $# $*"
  shift
done
exit 0
#2) comptador decrementa valor rebut
num=$1
MIN=0
while [ $num -ge $MIN ]
do
 echo "$num"
 ((num--))
done
exit 0
#1) mostrar numeros del 1 al 10
MAX=10
num=1
while [ $num -le $MAX ]
do
  echo "$num "
  ((num++))
done
exit 0
