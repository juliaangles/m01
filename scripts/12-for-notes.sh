#! /bin/bash
#@julia ASIX-M01
#Gener 2024
#Programa notes: Programa que rep almenys una nota o més  i per cada nota diu si és un #suspès, aprovat, notable o excel·lent.
#
#------------------------------------------------------------------------------------

ERR_NARGS=1
ERR_ARGVL=2

#1) Validar arguments

if [ $# -eq 0 ]
then
  echo "Error: num args incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi

#2)Validar rang de nota
#error recuperable ja que encara que el primer no sigui valid hem de seguir mirant 
#els altres.

for nota in $*
do
	if ! [ $nota -ge 0 -a $nota -le 10 ]
	then
		echo "Error: nota $nota no vàlid, la nota ha de estar entre (0-10)" >> /dev/stderr
	else
		if [ $nota -lt 5 ]
		then
			echo "Nota $nota: suspès"
		elif [ $nota -lt 7 ]
		then
	  		echo "Nota $nota: aprovat"
		elif [ $nota -lt 9 ]
		then
 			echo "Nota $nota: notable"      	
		else
			echo "Nota $nota: excel·lent"
		fi
	fi
done
exit 0
