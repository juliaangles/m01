#! /bin/bash
#@ julia ASIX-M01 Curs 2023-2024
#Febrer 2024
#llistar el directori rebut
#	a) verificar rep un arg
#	b) verificar que es un directori
#
#1) verificar rep un arg

ERR_ARGS=1
ERR_NODIR=2

if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_ARGS
fi

#2) verificar que es un directori
if  [ ! -d $1 ]
then
	echo "Error: $1 no es un directori"
	echo "Usage: $0 dir"
	exit $ERR_NODIR
fi

#3) xixa
 $1
 exit 0

