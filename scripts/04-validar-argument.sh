#! /bin/bash
#
#
#Validar que té exàctament 2 args i mostrar-los
#
#04-validar-arguments nom cognom
#
#1)valida que hi ha 2 args

if [ $# -ne 2 ]
then
	echo "Error: num d'arguments incorrecte"
	echo "Usage: $0 nomcognom"
	exit 1
fi

#
#2)xixa que els mostra

nom=$1
cognom=$2

echo "El nom es $1 $2"

exit 0
