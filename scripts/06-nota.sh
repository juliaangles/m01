#! /bin/bash
#@ julia ASIX-M01 Curs 2023-2024
#Febrer 2024
#Validar nota: suspes, aprovat
#
#
#a) rep un argument
ERR_NARGS=1
ERR_NOTA=2
if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_NARGS
fi

#b) es del 0 al 10
nota=$1

if ! [ $1 -ge 0 -a $1 -le 10 ]
then
    echo "Error: nota $1 no valida"
    echo "nota pren valor del 0 al 10"
    echo "Usage: $0 nota"
    exit $ERR_NOTA
fi

#c) xixa
nota=$1
if [ $nota -lt 5 ]
then
    echo "Nota $1: suspes"
elif [ $nota -le 6 ]
then
    echo "Nota $1:be"
elif [ $nota -le 8 ]
then
    echo "Nota $1:notable"

else
    echo "Nota $1:excelent"
fi
exit 0
