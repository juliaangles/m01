#! /bin/bash
#@ julia ASIX-M01 
#Curs 2023-2024
#Febrer 2024
#Exemples case
#-----------------------
#2)dl dt dc dj dv ----> laborals
#ds dm ---> festiu
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
	  echo "$1 es un dia entre setmana"
	  ;;
  "ds"|"dm")
	echo "$1 cap de setmana"
	;;
	*)
	echo "aixo ($1) no es un dia de la setmana";;	

esac
exit 0

#1 exemple vocals
case $1 in
  [aeiou])
    echo "$1 es una vocal"
    ;;
   [bcdfghjklmnpqrstwvxyz])
    ;;
    echo "$1 es una consonant"
  *)
    echo "$1 es una altre cosa"
    ;;
esac
exit 0
